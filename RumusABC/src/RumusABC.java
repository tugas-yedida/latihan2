import java.util.Scanner;
import java.lang.Math;

public class RumusABC {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        char ulang = 'Y'; //variabel untuk mengulang program
        while (ulang == 'Y' || ulang == 'y') { //looping selama ulang = Y atau y
            System.out.println("Program Menghitung Rumus ABC");
            System.out.println("Masukkan nilai a, b, dan c dari persamaan kuadrat aX2 + bX + C");
            System.out.print("a = ");
            double a = input.nextDouble(); //input nilai a
            System.out.print("b = ");
            double b = input.nextDouble(); //input nilai b
            System.out.print("c = ");
            double c = input.nextDouble(); //input nilai c
            double D = b * b - 4 * a * c; //menghitung determinan
            if (D > 0) { //jika D > 0 maka ada dua akar real
                double x1 = (-b - Math.sqrt(D)) / (2 * a); //menghitung x1
                double x2 = (-b + Math.sqrt(D)) / (2 * a); //menghitung x2
                System.out.println("Akar-akar persamaan kuadrat adalah:");
                System.out.println("x1 = " + x1);
                System.out.println("x2 = " + x2);
            } else if (D == 0) { //jika D = 0 maka ada satu akar real
                double x = -b / (2 * a); //menghitung x
                System.out.println("Akar-akar persamaan kuadrat adalah:");
                System.out.println("x1 = x2 = " + x);
            } else { //jika D < 0 maka ada dua akar imajiner
                double real = -b / (2 * a); //menghitung bagian real
                double imag = Math.sqrt(-D) / (2 * a); //menghitung bagian imajiner
                System.out.println("Akar-akar persamaan kuadrat adalah:");
                System.out.println("x1 = " + real + " - " + imag + "i");
                System.out.println("x2 = " + real + " + " + imag + "i");
            }
            System.out.print("Input data lagi [Y/T] ? "); //menanyakan apakah ingin mengulang program
            ulang = input.next().charAt(0); //input jawaban
        }
        input.close(); //menutup scanner
    }
}